package filmoteca.eps.ua.es.lectorhtml;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText url_edtxt;
    private TextView html_tv;
    private Button acces_bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_lector_html);
        url_edtxt = (EditText)findViewById(R.id.etUrl);
        html_tv = (TextView)findViewById(R.id.tvContenido);
        acces_bt = (Button)findViewById(R.id.btnAcceder);
        acces_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadHtmlTask().execute(url_edtxt.getText().toString());
            }
        });
    }

    private class DownloadHtmlTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            acces_bt.setEnabled(false);
        }

        @Override
        protected String doInBackground(String[] url) {
            return descargarContenido(url[0]);
        }

        @Override
        protected void onPostExecute(String content) {
            html_tv.setText(content);
            acces_bt.setEnabled(true);
        }

        @Override
        protected void onCancelled() {
            acces_bt.setEnabled(true);
        }
    }

    public String descargarContenido( String strUrl )
    {
        HttpURLConnection http = null;
        String content = null;

        try {
            URL url = new URL( strUrl );
            http = (HttpURLConnection)url.openConnection();

            if( http.getResponseCode() == HttpURLConnection.HTTP_OK ) {
                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader( http.getInputStream() ));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                content = sb.toString();
                reader.close();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if( http != null )
                http.disconnect();
        }
        return content;
    }

}
